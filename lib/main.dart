import 'package:bingo/app.dart';
import 'package:bingo/src/authentication/authentication_bloc.dart';
import 'package:bingo/src/loto/loto_api.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final GlobalKey<NavigatorState> _navigator = GlobalKey();

  @override
  Widget build(BuildContext context) {
    final authenticationBloc = AuthenticationBloc(LotoApi(Client()));
    return BlocProvider<AuthenticationBloc>(
      bloc: authenticationBloc,
      child: MaterialApp(
        navigatorKey: _navigator,
        title: 'Eesti loto',
        theme: ThemeData(
          buttonColor: Colors.yellow,
          primarySwatch: Colors.yellow,
          textTheme: _textTheme(),
        ),
        home: App(navigator: _navigator),
      ),
    );
  }

  _textTheme() {
    return TextTheme(
        title: TextStyle(fontSize: 18.0), body1: TextStyle(fontSize: 18.0));
  }
}
