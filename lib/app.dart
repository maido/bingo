import 'package:bingo/src/authentication/authentication_bloc.dart';
import 'package:bingo/src/authentication/authentication_event.dart';
import 'package:bingo/src/authentication/authentication_state.dart';
import 'package:bingo/src/login/login.dart';
import 'package:bingo/template.dart';
import 'package:bingo/tickets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class App extends StatelessWidget {
  final GlobalKey<NavigatorState> navigator;

  const App({this.navigator});

  @override
  Widget build(BuildContext context) {
    final authBloc = BlocProvider.of<AuthenticationBloc>(context);
    return BlocBuilder<AuthenticationEvent, AuthenticationState>(
        bloc: authBloc,
        builder: (context, state) {
          if (state.isLoading) {
            return Center(child: CircularProgressIndicator());
          }
          if (state.apiToken != null) {
            return Template(child: Tickets(navigator: navigator));
          }
          return Login();
        });
  }
}
