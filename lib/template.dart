import 'package:bingo/src/authentication/authentication_bloc.dart';
import 'package:bingo/src/authentication/authentication_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Template extends StatelessWidget {
  final Widget child;

  const Template({this.child});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(actions: <Widget>[
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              final authBloc = BlocProvider.of<AuthenticationBloc>(context);
              authBloc.dispatch(AuthenticationLogOutEvent());
            },
          ),
          // overflow menu
        ], title: Image.asset("assets/eestiloto.png"), centerTitle: true),
        body: child);
  }
}
