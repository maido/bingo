import 'package:bingo/src/authentication/authentication_bloc.dart';
import 'package:bingo/src/loto/loto_api.dart';
import 'package:bingo/src/loto/ticket.dart';
import 'package:bingo/src/loto/tickets_bloc.dart';
import 'package:bingo/src/loto/tickets_event.dart';
import 'package:bingo/src/loto/tickets_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart';

class Tickets extends StatefulWidget {
  final GlobalKey<NavigatorState> navigator;

  const Tickets({this.navigator});

  @override
  _TicketsState createState() => _TicketsState();
}

class _TicketsState extends State<Tickets> {
  TicketsBloc _ticketsBloc;

  @override
  void initState() {
    _ticketsBloc = TicketsBloc(LotoApi(Client()), widget.navigator);
    super.initState();
  }

  @override
  void dispose() {
    _ticketsBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final authBloc = BlocProvider.of<AuthenticationBloc>(context);
    return BlocBuilder<TicketsEvent, TicketsState>(
      bloc: _ticketsBloc,
      builder: (context, state) {
        final tickets = state.tickets;
        if (tickets == null) {
          _ticketsBloc
              .dispatch(LoadTicketsEvent(authBloc.currentState.apiToken));
          return Center(child: CircularProgressIndicator());
        }
        return Container(
          child: ListView.builder(
              itemCount: tickets.length,
              itemBuilder: (context, i) {
                final ticket = tickets[i];
                return ListTile(
                  title: Text(
                    ticket.name,
                    style: Theme.of(context)
                        .textTheme
                        .title
                        .copyWith(color: ticket.textColor),
                  ),
                  subtitle: Text(ticket.id),
                  onTap: () async {
                    if (ticket.type == TicketType.bingo) {
                      _ticketsBloc.dispatch(LoadFieldsEvent(
                          authBloc.currentState.apiToken, ticket));
                    } else {
                      Scaffold.of(context).showSnackBar(SnackBar(
                          content:
                              Text("This ticket type is not supported yet!")));
                    }
                  },
                );
              }),
        );
      },
    );
  }
}
