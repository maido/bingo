import 'package:bingo/src/authentication/authentication_event.dart';
import 'package:bingo/src/authentication/authentication_state.dart';
import 'package:bingo/src/loto/loto_api.dart';
import 'package:bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final LotoApi _lotoApi;

  AuthenticationBloc(this._lotoApi) {
    _init();
  }

  @override
  AuthenticationState get initialState => AuthenticationState.initial();

  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationState currentState, AuthenticationEvent event) async* {
    if (event is AuthenticationSuccessfulEvent) {
      yield AuthenticationState.success(event.token);
    } else if (event is AuthenticationInitDoneEvent) {
      yield AuthenticationState.done();
    } else if (event is AuthenticationLogOutEvent) {
      final pref = await SharedPreferences.getInstance();
      await pref.clear();
      yield AuthenticationState.done();
    }
  }

  void _init() async {
    final pref = await SharedPreferences.getInstance();
    final username = pref.getString("username");
    final password = pref.getString("password");
    if (username != null && password != null) {
      print("Initialising token from prefs");
      final token = await _lotoApi.login(username, password);
      dispatch(AuthenticationSuccessfulEvent(token));
    } else {
      dispatch(AuthenticationInitDoneEvent());
    }
  }
}
