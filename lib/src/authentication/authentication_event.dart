import 'package:bingo/src/loto/api_token.dart';

abstract class AuthenticationEvent {}

class AuthenticationSuccessfulEvent extends AuthenticationEvent {
  final ApiToken token;

  AuthenticationSuccessfulEvent(this.token);
}

class AuthenticationInitDoneEvent extends AuthenticationEvent {}

class AuthenticationLogOutEvent extends AuthenticationEvent {}
