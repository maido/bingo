import 'package:bingo/src/loto/api_token.dart';

class AuthenticationState {
  final ApiToken apiToken;
  final bool isLoading;

  const AuthenticationState(this.apiToken, this.isLoading);

  bool hasToken() {
    return apiToken != null;
  }

  factory AuthenticationState.initial() {
    return AuthenticationState(null, true);
  }

  factory AuthenticationState.done() {
    return AuthenticationState(null, false);
  }

  factory AuthenticationState.success(ApiToken token) {
    return AuthenticationState(token, false);
  }
}
