import 'package:bingo/src/login/login_event.dart';
import 'package:bingo/src/login/login_state.dart';
import 'package:bingo/src/loto/loto_api.dart';
import 'package:bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final LotoApi lotoApi;

  LoginBloc(this.lotoApi);

  @override
  LoginState get initialState => LoginState.initial();

  void onLoginButtonPressed({String username, String password}) {
    dispatch(
      LoginButtonPressed(
        username: username,
        password: password,
      ),
    );
  }

  @override
  Stream<LoginState> mapEventToState(
      LoginState currentState, LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield LoginState.loading();

      try {
        final token = await lotoApi.login(event.username, event.password);
        final pref = await SharedPreferences.getInstance();
        pref.setString("username", event.username);
        pref.setString("password", event.password);
        yield LoginState.success(token);
      } catch (error) {
        yield LoginState.failure(error.toString());
      }
    } else if (event is LoggedIn) {
      yield LoginState.initial();
    }
  }
}
