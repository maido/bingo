import 'package:bingo/src/authentication/authentication_bloc.dart';
import 'package:bingo/src/authentication/authentication_event.dart';
import 'package:bingo/src/login/login_bloc.dart';
import 'package:bingo/src/login/login_event.dart';
import 'package:bingo/src/login/login_state.dart';
import 'package:bingo/src/loto/loto_api.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart';

class Login extends StatefulWidget {
  const Login({Key key}) : super(key: key);

  @override
  _LoginState createState() {
    return new _LoginState();
  }
}

class _LoginState extends State<Login> {
  LoginBloc _loginBloc;
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  void initState() {
    _loginBloc = LoginBloc(LotoApi(Client()));
    super.initState();
  }

  @override
  void dispose() {
    _loginBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset("assets/eestiloto.png"),
          BlocBuilder<LoginEvent, LoginState>(
            bloc: _loginBloc,
            builder: (context, state) {
              if (state.token != null) {
                final authBloc = BlocProvider.of<AuthenticationBloc>(context);
                authBloc.dispatch(AuthenticationSuccessfulEvent(state.token));
                _loginBloc.dispatch(LoggedIn());
              }
              return Column(
                children: <Widget>[
                  TextField(
                    decoration: InputDecoration(labelText: "Kasutajanimi"),
                    controller: _usernameController,
                  ),
                  TextField(
                    decoration: InputDecoration(labelText: "Parool"),
                    controller: _passwordController,
                    obscureText: true,
                  ),
                  RaisedButton(
                    child: Text('SISENE'),
                    onPressed: () {
                      _loginBloc.dispatch(LoginButtonPressed(
                          username: _usernameController.text,
                          password: _passwordController.text));
                    },
                  ),
                ],
              );
            },
          ),
        ],
      ),
    ));
  }
}
