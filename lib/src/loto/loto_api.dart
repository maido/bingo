import 'package:bingo/src/loto/api_token.dart';
import 'package:bingo/src/loto/ticket.dart';
import 'package:html/dom.dart';
import 'package:html/parser.dart' show parse;
import 'package:http/http.dart';

class LotoApi {
  static const String baseUrl = 'https://www.eestiloto.ee';
  static const String actionToken =
      r'org\.apache\.struts\.action\.TOKEN\=(.{32})';
  static const String taglibToken =
      r'org\.apache\.struts\.taglib\.html\.TOKEN\=(.{32})';
  static RegExp actionRegexp = new RegExp(actionToken);
  static RegExp taglibRegexp = new RegExp(taglibToken);

  Client _client;

  LotoApi(this._client);

  Future<ApiToken> login(String username, String password) async {
    print("Logging in user $username with password $password");
    var _token = await getCsrfToken();
    print('Initial token: $_token');
    final response = await _client.post(
        "$baseUrl/osi.do?org.apache.struts.taglib.html.TOKEN=$_token",
        body: {
          "action": "login",
          "username": username,
          "password": password,
          "page": "1",
        });
    final location = response.headers['location'];
    _token = actionRegexp.firstMatch(location).group(1);
    print('Second token: $_token');
    final sessionId =
        response.headers['set-cookie'].split(';')[0].split("=")[1];
    var _sessionId = sessionId;
    print('Session id is $_sessionId');
    return ApiToken(_token, _sessionId);
  }

  Future<List<Ticket>> getTickets(ApiToken token) async {
    final response = await _client.get(
        '$baseUrl/osi/tickets.do?onlyActive=false&org.apache.struts.action.TOKEN=${token.token}',
        headers: {'Cookie': 'JSESSIONID=${token.sessionId}'});
    final document = parse(response.body);
    final _token = await getActionToken(document);
    print('Token: $_token');
    token.updateToken(_token);
    final table = document.getElementsByClassName('main_table_layout')[0];
    final tableRows = table.getElementsByTagName('tr');
    tableRows.removeAt(0);
    return tableRows.map((row) {
      final ticketId = row
          .getElementsByTagName('td')[0]
          .getElementsByTagName('a')[0]
          .innerHtml;
      return Ticket(ticketId, _getTicketType(row));
    }).toList();
  }

  TicketType _getTicketType(Element tableRow) {
    final secondTdImg = tableRow.children[1].children[0];
    if (secondTdImg.classes.contains('cart-bingo')) {
      return TicketType.bingo;
    } else if (secondTdImg.classes.contains('cart-viking2')) {
      return TicketType.viking;
    } else if (secondTdImg.classes.contains('cart-genoa')) {
      return TicketType.eurojackpot;
    }
    return TicketType.unknown;
  }

  Future<List<Field>> getFields(ApiToken token, Ticket ticket) async {
    final response = await _client.get(
        '$baseUrl/osi/tickets.do?onlyActive=false&pageNumber=1&gameSortType=0&sortProperty=ISSUE_DATE&ticketID=${ticket.id}&org.apache.struts.action.TOKEN=${token.token}',
        headers: {'Cookie': 'JSESSIONID=${token.sessionId}'});
    final document = parse(response.body);
    final _token = await getActionToken(document);
    print('Token: $_token');
    token.updateToken(_token);
    final modalTicket = document.getElementById('modalTicket');
    final ticketType = _getTicketTypeFromElement(modalTicket);
    if (ticketType != TicketType.bingo) {
      throw Exception("Ticket type: $ticketType not supported yet!");
    }
    final numbersBox =
        document.getElementsByClassName('ticket_game_draw_wrapper')[0];
    final fields = numbersBox.getElementsByClassName('bingo_ticket');

    return fields.map((e) => _extractField(e)).toList();
  }

  TicketType _getTicketTypeFromElement(Element modalTicket) {
    final logoSrc =  modalTicket.getElementsByClassName("modal_logo")[0].attributes["src"];
    if (logoSrc.contains('bingo')) {
      return TicketType.bingo;
    } else if (logoSrc.contains('viking')) {
      return TicketType.viking;
    } else if (logoSrc.contains('euro')) {
      return TicketType.eurojackpot;
    }
    return TicketType.unknown;
  }

  Future<String> getCsrfToken() async {
    final result = await _client.get(baseUrl);
    final document = parse(result.body);
    final links = document.getElementsByTagName('a');
    final text = links[5].attributes['href'];
    return taglibRegexp.firstMatch(text).group(1);
  }

  Future<String> getActionToken(Document document) async {
    final links = document.getElementsByTagName('a');
    final text = links[5].attributes['href'];
    return actionRegexp.firstMatch(text).group(1);
  }

  Field _extractField(Element e) {
    final columns = e.getElementsByClassName('bingo_ticket_row');
    final numbers = columns.map((c) => _extractNumbers(c)).toList();
    return Field(_transformColumnsToRows(numbers.expand((i) => i).toList()));
  }

  List<int> _extractNumbers(Element c) {
    return c
        .getElementsByClassName('ticket_column')
        .where((td) => !td.classes.contains('ticket_header'))
        .map((td) => int.parse(td.children[0].innerHtml))
        .toList();
  }

  List<int> _transformColumnsToRows(List<int> numbers) {
    final result = List<int>.from(numbers);
    for (var n = 0; n < 4; n++) {
      for (var m = n + 1; m < 5; m++) {
        final i = n * 5 + m;
        final j = m * 5 + n;
        result[j] = numbers[i];
        result[i] = numbers[j];
      }
    }
    return result;
  }
}
