import 'dart:convert';

import 'package:bingo/board.dart';
import 'package:bingo/src/loto/loto_api.dart';
import 'package:bingo/src/loto/ticket.dart';
import 'package:bingo/src/loto/tickets_event.dart';
import 'package:bingo/src/loto/tickets_state.dart';
import 'package:bingo/template.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TicketsBloc extends Bloc<TicketsEvent, TicketsState> {
  final LotoApi _lotoApi;
  final GlobalKey<NavigatorState> _navigator;

  TicketsBloc(this._lotoApi, this._navigator);

  @override
  TicketsState get initialState => TicketsState.initial();

  @override
  Stream<TicketsState> mapEventToState(
      TicketsState currentState, TicketsEvent event) async* {
    print("Ticket event $event");
    if (event is LoadTicketsEvent) {
      final tickets = await _lotoApi.getTickets(event.token);
      print(tickets);
      yield TicketsState.withTickets(tickets);
    } else if (event is LoadFieldsEvent) {
      final fields = await _lotoApi.getFields(event.token, event.ticket);
      print("Building board with fields: $fields");
      _navigator.currentState.push(MaterialPageRoute(builder: (context) {
        return BlocProvider<TicketsBloc>(
            child: Template(child: Board(ticket: event.ticket, fields: fields)),
            bloc: this);
      }));
      final selected = await getSelected(event.ticket, fields);
      yield currentState.withFields(event.ticket, fields, selected);
    } else if (event is FieldSelectedEvent) {
      final state = currentState.select(event.field, event.number);
      _persist(state.ticket, state.selected);
      yield state;
    }
  }

  Future<Selected> getSelected(Ticket ticket, List<Field> fields) async {
    final pref = await SharedPreferences.getInstance();
    final selectedState = pref.getString(ticket.id);
    if (selectedState != null) {
      final jsonValue = json.decode(selectedState);
      return Selected.fromJson(jsonValue);
    } else {
      return Selected();
    }
  }

  void _persist(Ticket ticket, Selected selected) async {
    final pref = await SharedPreferences.getInstance();
    final jsonValue = json.encode(selected.toJson());
    pref.setString(ticket.id, jsonValue);
  }
}
