import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:quiver_hashcode/hashcode.dart';

enum TicketType { bingo, viking, keno, eurojackpot, unknown }

class Ticket {
  final String id;
  final TicketType type;

  Ticket(this.id, this.type);

  String get name {
    if (type == TicketType.bingo) {
      return "Bingo loto";
    } else if (type == TicketType.eurojackpot) {
      return "Eurojackpot";
    } else if (type == TicketType.viking) {
      return "Vikinglotto";
    } else if (type == TicketType.keno) {
      return "Keno loto";
    }
    return "Unknown";
  }

  Color get textColor {
    switch (type) {
      case TicketType.bingo:
        return Colors.yellow;
      case TicketType.viking:
        return Colors.blue;
      case TicketType.eurojackpot:
        return Colors.yellow;
      case TicketType.keno:
        return Colors.red;
      case TicketType.unknown:
        return Colors.black;
    }
    throw Exception();
  }

  @override
  int get hashCode => hash2(id, type);

  @override
  bool operator ==(other) {
    if (other is Ticket) {
      return other.type == this.type && other.id == this.id;
    }
    return false;
  }
}

class Field {
  final List<int> numbers;

  Field(this.numbers);

  @override
  int get hashCode => hashList(numbers);

  @override
  bool operator ==(other) {
    if (other is Field) {
      return _listsAreEqual(other.numbers, this.numbers);
    }
    return false;
  }

  bool _listsAreEqual(list1, list2) {
    var i = -1;
    return list1.every((val) {
      i++;
      if (val is List && list2[i] is List)
        return _listsAreEqual(val, list2[i]);
      else
        return list2[i] == val;
    });
  }

  @override
  String toString() {
    return numbers.join(',');
  }
}
