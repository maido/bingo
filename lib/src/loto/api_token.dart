class ApiToken {
  String token;
  String sessionId;

  ApiToken(this.token, this.sessionId);

  ApiToken updateToken(String token) {
    this.token = token;
    return this;
  }

  ApiToken updateSession(String sessionId) {
    this.sessionId = sessionId;
    return this;
  }
}
