import 'package:bingo/src/loto/ticket.dart';

class TicketsState {
  List<Ticket> tickets;
  Ticket ticket;
  List<Field> fields;
  Selected selected;

  TicketsState(this.tickets, this.ticket, this.fields, this.selected);

  factory TicketsState.initial() {
    return TicketsState(null, null, null, null);
  }

  factory TicketsState.withTickets(List<Ticket> tickets) {
    return TicketsState(tickets, null, null, null);
  }

  TicketsState withFields(
      Ticket ticket, List<Field> fields, Selected selected) {
    return TicketsState(this.tickets, ticket, fields, selected);
  }

  TicketsState select(int fieldNr, int number) {
    selected.select(fieldNr, number);
    return TicketsState(tickets, ticket, fields, selected);
  }
}

class Selected {
  List<int> selected1 = List.filled(76, -1);
  List<int> selected2 = List.filled(76, -1);

  Selected();

  Selected.fromJson(Map<String, dynamic> json)
      : selected1 =
            json['selected1'].split(',').map<int>((i) => int.parse(i)).toList(),
        selected2 =
            json['selected2'].split(',').map<int>((i) => int.parse(i)).toList();

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'selected1': selected1.join(','),
      'selected2': selected2.join(',')
    };
  }

  void select(int field, int number) {
    if (field == 0) {
      selected1[number] = -selected1[number];
    } else if (field == 1) {
      selected2[number] = -selected2[number];
    }
  }

  bool isSelected(int field, int number) {
    if (field == 0) {
      return selected1[number] == 1;
    } else if (field == 1) {
      return selected2[number] == 1;
    }
    throw Exception();
  }
}
