import 'package:bingo/src/loto/api_token.dart';
import 'package:bingo/src/loto/ticket.dart';

abstract class TicketsEvent {}

class LoadTicketsEvent extends TicketsEvent {
  ApiToken token;

  LoadTicketsEvent(this.token);
}

class LoadFieldsEvent extends TicketsEvent {
  ApiToken token;
  Ticket ticket;

  LoadFieldsEvent(this.token, this.ticket);
}

class FieldSelectedEvent extends TicketsEvent {
  int field;
  int number;

  FieldSelectedEvent(this.field, this.number);
}

class InitTickets extends TicketsEvent {
  String ticketsState;

  InitTickets(this.ticketsState);
}
