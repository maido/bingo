import 'package:bingo/src/loto/ticket.dart';
import 'package:bingo/src/loto/tickets_bloc.dart';
import 'package:bingo/src/loto/tickets_event.dart';
import 'package:bingo/src/loto/tickets_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Board extends StatelessWidget {
  final Ticket ticket;
  final List<Field> fields;

  const Board({this.ticket, this.fields});

  @override
  Widget build(BuildContext context) {
    final content = new List<Widget>.generate(fields.length, (i) {
      final field = fields[i];
      return GridView.count(
        shrinkWrap: true,
        crossAxisCount: 5,
        childAspectRatio: 1.5,
        padding: EdgeInsets.symmetric(horizontal: 50.0),
        children: new List<Widget>.generate(field.numbers.length, (j) {
          final number = field.numbers[j];
          return _buildNumber(context, i, number);
        }),
      );
    });
    final bingo = "BINGO";
    return Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
              GridView.count(
                crossAxisCount: 5,
                shrinkWrap: true,
                childAspectRatio: 1.5,
                padding: EdgeInsets.symmetric(horizontal: 50.0),
                children: List<Widget>.generate(bingo.length, (i) {
                  return GridTile(
                    child: Center(
                      child: Text(bingo[i]),
                    ),
                  );
                }),
              )
            ] +
            content);
  }

  Widget _buildNumber(BuildContext context, int field, int number) {
    final ticketsBloc = BlocProvider.of<TicketsBloc>(context);
    return GestureDetector(
      onTap: () {
        ticketsBloc.dispatch(FieldSelectedEvent(field, number));
      },
      child: GridTile(
          child: BlocBuilder<TicketsEvent, TicketsState>(
        bloc: ticketsBloc,
        builder: (context, state) {
          if (state.fields == null) {
            return Center(child: CircularProgressIndicator());
          }
          return Container(
              color: state.selected.isSelected(field, number)
                  ? Colors.red
                  : Colors.yellow,
              child: Center(child: Text(number.toString())));
        },
      )),
    );
  }
}
